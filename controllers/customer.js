exports.create = async function(req, res, next) {
  var customersParams = {
    customerNumber: req.body.customerNumber,
    name: req.body.name
  }

  let addCustomer = await users.add(customersParams);
  if (!addCustomer) {
      return res.status(500).json({ status:'error', message: 'Something gone wrong when creating customer' });
  }
  return res.status(200).json({ status:'success',data: addCustomer });

};